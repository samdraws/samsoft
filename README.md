# SAM SOFTWARE
A home made machine for computational drawing

[![PyPI](https://img.shields.io/pypi/l/fsfe-reuse.svg)](https://www.gnu.org/licenses/gpl-3.0.html)
[![reuse compliant](https://img.shields.io/badge/reuse-compliant-green.svg)](https://git.fsfe.org/reuse/reuse)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
---

![SAM_02.jpg](https://bitbucket.org/repo/8zaXopr/images/2194714084-SAM_02.jpg)

## Presentation

SAM is a drawing machine that I made with one of my students from ESAD d'Amiens as part of his internship. 
We set out in 2014 to make from scratch a flat table design inspired by the algorithmic pioneers of the sixties and proceed to explore its possibilities for drawing. We presented this work at the Nuit Blanche in 2014. Since that first appearance, SAM has had the opportunity to work with students in Bordeaux as well as entertain kids during a festival at the prestigious Centre Pompidou in Paris.
Source code for some of the programs used in SAM's projects can be found in this repository.


## Repository Contents

* DEV
* MOLLAT
* BOBO


## Projects & Workshops

* [Official website](https://samdraws.bitbucket.io/)

* [SAM@BoBo](https://samdraws.bitbucket.io/logs/log012.html)
	An art installation for the Centre Pompidou in Paris that took place during their second edition of La F�te du Code in 2017.
* [CODEX � Bordeaux](https://samdraws.bitbucket.io/logs/log011.html)
	Codex was a one week residency and workshop with students from ESAV Bordeaux.
* [Article: personal website](https://area03.bitbucket.io/projects/samdraws.html)

* [Photos](https://www.flickr.com/photos/tisane_01/albums/72157645344025569)


## Install

The programs found in this repository can be compiled using the Processing & Arduino environments.

* [https://processing.org/](https://processing.org/)
* [http://arduino.cc/](http://arduino.cc/)

## Contact & Sundries

* mark.webster[at]wanadoo.fr
* Version v1.0
* Tools used : Processing, Arduino

## Contribute
To contribute to this project, please contact me at the above email address.

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

For more information https://www.gnu.org/licenses/gpl-3.0.en.html

The program in this repository meet the requirements to be REUSE compliant,
meaning its license and copyright is expressed in such as way so that it
can be read by both humans and computers alike.

For more information, see https://reuse.software/
